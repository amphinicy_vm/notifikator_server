# README #



### What is this repository for? ###

* This repo consists of server side background scripts, services and config files.
* v1.0

### How do I get set up? ###

#### Server Setup ####

We will use raspbian as it is official supported rPi operating system.
If you wish to deploy the server on a different system just follow general steps.

*  [DOWNLOAD](https://www.raspberrypi.org/downloads/raspbian/) and install Raspbian to your rPi. (You can use lite version)

* Nice tool to copy the image to SD card is [Etcher](https://etcher.io/)

* Before you put the SD card to your rPi add the ssh file to the root of the card. **IMAGE HERE**

* The easiest way to find your IP address is to use "Network scanner" app (e.g. [**Android**](https://play.google.com/store/apps/details?id=com.myprog.netscan&hl=en), [**iOS**](https://itunes.apple.com/us/app/fing-network-scanner/id430921107?mt=8)) - **IMAGE HERE**

##### ssh credentials: #####

	user:		pi
	password:	raspberry

**NOTE:** these are the default credentials! Change the password before you proceed!!!

##### nginx #####

Now that you can ssh to your box you can install the nginx which we will use to serve our static files.
Here is a nice, short and official [GUIDE](https://www.raspberrypi.org/documentation/remote-access/web-server/nginx.md).

	pi@raspberrypi:~ $ nginx -v
	nginx version: nginx/1.10.3

edit nginx conf - DODAJ PRIMJER FILEA

##### NODE #####

To update debian apt package repo run

	curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

Install Node

	sudo apt-get install nodejs
	pi@raspberrypi:~ $ node -v
	v8.7.0


Create folder ==*/usr/share/Notifikator/Notifikator_server_scripts*== to hold all the neccessary scripts, config files and "database".

DOWNLOADING repo

The easiest way is to download repo directly to your server by using 

    wget https://bitbucket.org/amphinicy_vm/notifikator_server/get/21a7d2f251ec.zip -O /tmp/source.zip

NOTE: get the latest link from the Download section

Unzip the content 

    unzip source.zip

Copy everything from ==*Notifikator_server_scripts*== to ==*/usr/share/Notifikator/Notifikator_server_scripts*==.

Run: 

	cd /usr/share/Notifikator/Notifikator_server_scripts
    sudo npm install
	
### Known issues ###

* groups file gets corupted
	*	open the file and make some changes
	*	if you need the template for the file you can find it at examples/groups_bkp.json
	*	save changes
* reorder people
	*	open file
	*	manually edit file
	*	save changes