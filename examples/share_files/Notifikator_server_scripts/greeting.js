var fs = require("fs");

exports.getRandomImage = function () {
	var images = fs.readdirSync("/var/www/client/images/funny/");
	var image = images[Math.floor(Math.random() * images.length)];
    return image;
};