const express = require('express');
const app = express();

app.get('/', function (req, res) {
  res.send('It\'s working!');
})

app.listen(5555, function () {
  console.log('Test is listening on port 5555!');
})