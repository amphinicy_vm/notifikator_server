const LOG_FILE = '/var/log/notifikator.log'; 
const GREETING_DELAY = 30; // in seconds
const CLEAR_GROUP_IN_MINUTES = 30;
const GROUPS_CONFIG_FILE = "/usr/share/Notifikator/groups.json";
const WEEK_MENU_FILE = "/usr/share/Notifikator/weekmenu.html";
const DAILY_NEWS_FILE = "/usr/share/Notifikator/dailynews.html";
const MONEY_FILE = "/usr/share/Notifikator/money.json";
const CONFIG_FILE = "/usr/share/Notifikator/Notifikator_server_scripts/config.json";
const HASH_FILE_PATH = "/usr/share/Notifikator/hash/";
const PORT = 3001;

var express = require('express');
var http = require('http');
var crypto = require('crypto');

var greeting = require('./greeting');
var fs = require("fs");
var _ = require('lodash');
var app = express();
var server = http.createServer(app);
var io = require('socket.io')(server);
var config;
var activeGroup = {
		name: "", 
		time: undefined
	};

if (!fs.existsSync(HASH_FILE_PATH)){
	fs.mkdirSync(HASH_FILE_PATH);
}

// load config
fs.readFile(CONFIG_FILE, function(err, data) {
	if(err) {
		logIt("Loading config FAILED");
		logIt(err);
		fs.closeSync(fs.openSync(CONFIG_FILE, 'w'));
	} else {
		logIt("Loading config OK");
		config = safeJsonParse(CONFIG_FILE, data);
		logIt(JSON.stringify(config));
	}
});

io.on('connection', function(client){
	logIt('Client connected');
	notifyAdmin();

	// Wish him/her a nice day
	// Give a few seconds delay so user can join with his name
	setTimeout(function(){
		var currentDate = new Date();
		var currentHour = currentDate.getHours();
		var adaptiveTitle = 'Dobro jutro!';
		if (currentHour > 11) { adaptiveTitle = 'Dobar dan!'; }
		if (currentHour > 18) { adaptiveTitle = 'Dobro večer!'; }
		var message = {
				title: adaptiveTitle, 
				message: 'Želim ti uspješan dan!', 
				image: 'http://192.168.210.140/images/funny/' + greeting.getRandomImage(),
				from: "admin"
		};
		if ( boolValue(client.sendWellcomeMessage) ) {
			//client.emit('notification', message);
			//logIt('[emit] to client:' + client.name + ' ::: ' + JSON.stringify(message));
		}
	}, GREETING_DELAY * 1000);

	client.on('welcomemessagesetting', function(data){
		client.sendWellcomeMessage = data;
	});

	client.on('join', function(name){
		if ( name === "Admin") {
			logIt("Generating random string for admin login...");
			var randstr = Math.random().toString(36).substr(2);
			var hash = crypto.createHash('sha256').update(config.passcode+randstr).digest('hex');
			var ttl = new Date();
			ttl.setDate(ttl.getDate() + 1);
			var hashFileContent = {
				ttl: ttl
			};
			fs.writeFile(HASH_FILE_PATH + hash, JSON.stringify(hashFileContent, null, '\t'), function(err) {
				if(err) {
					return logIt(err);
				}
				logIt(HASH_FILE_PATH + hash + ' file created!');
			});
			client.emit('randstr', randstr);
			return;
		} else if ( typeof name === 'object' && name && name.name === 'Admin' ) {
			logIt("Hash: " + name.hash);
			fs.open(HASH_FILE_PATH + name.hash, 'r', (err, fd) => {
				if (err) {
					if (err.code === 'ENOENT') {
						logIt('[ERROR] hash not accepted');
						client.emit('loginfailed', 'Passcode wrong!');
						return;
					}
					throw err;
				}

				fs.readFile(GROUPS_CONFIG_FILE, function(err, data) {
					if(err) {
						logIt('[ERROR] '+err);
						client.emit('lunchgroups', {});
						fs.closeSync(fs.openSync(GROUPS_CONFIG_FILE, 'w'));
					} else {
						data = safeJsonParse(GROUPS_CONFIG_FILE, data);
						client.emit('lunchgroups', data);
					}
				});	
				client.name = name.name;
				authenticateAdmin(client);
				let adminSettings = getAdminSettings(client);
				notifyAdmin();
			});
		} else {
			logIt('Client set his name to: ' + name);
			logIt("Name: " + name);
			client.name = name;
			notifyAdmin();
			yourMoneyStatus(name);
		}
	});

	let settings = getSettings(client);

	client.on('messages', function(data){
		logIt('[messages] from: ' + client.name + ' ::: ' + JSON.stringify(data));
		var recepients = data.recepients;
		var notification = {};
		if ( isAdmin(client) ) {
			notification = {
				title: data.title,
				message: data.message,
				image: data.image,
				from: data.from
			};
			// At the moment we are iterating it over all clients and sending to ones that are on the list
			for ( let client_id of Object.keys(io.sockets.connected) ) {
				if ( _.includes(recepients, io.sockets.connected[client_id].name ) ) {
					io.sockets.connected[client_id].emit('notification', notification);
					logIt('[broadcast] to ' + io.sockets.connected[client_id].name + ' ::: ' + JSON.stringify(notification));
				}
			}

			if ( data.grupa ) {
				var group = data.grupa.toUpperCase();
				group = group.replace("GROUP", "").trim();
				client.broadcast.emit('activeLunchGroup', group);
				activeGroup = {name: group, time: Date.now()};
				setTimeout(function(){
					clearActiveGroup(client);
				}, (CLEAR_GROUP_IN_MINUTES * 60) * 1000);
			}
		} else {
			notification = {
				title: "Error!!!",
				message: "You are not authorized to send messages!",
				image: "/images/error.jpg",
				from: "admin"
			};
			client.emit('notification', notification);
			logIt('[emit] to: ' + client.name + ' ::: ' + JSON.stringify(notification));
		}
	});

	client.on('payforlunch', function(data){
		logIt('[payforlunch] from: ' + client.name + ' ::: ' + JSON.stringify(data));

		var notification = {};
		if ( isAdmin(client) ) {
			var money;
			try {
				money = fs.readFileSync(MONEY_FILE);
				money = JSON.parse(money);
				money[data.month][data.name] = data.status;
			} catch (err){
				logIt("[ERROR] " + err);
				fs.closeSync(fs.openSync(MONEY_FILE, 'w'));
			}

			fs.writeFile(MONEY_FILE, JSON.stringify(money, null, '\t'), function(err) {
				if(err) {
					return logIt(err);
				}
				logIt('MONEY_FILE file was edited!');
				getAdminSettings(client, data.month);
				// Notify client
				yourMoneyStatus(data.name);
			});
		} else {
			notification = {
				title: "Error!!!",
				message: "You are not authorized to manipulate with the money!",
				image: "/images/error.jpg",
				from: "admin"
			};
			client.emit('notification', notification);
			logIt('[emit] to: ' + client.name + ' ::: ' + JSON.stringify(notification));
		}
	});

	client.on('loadmonth', function(data){
		logIt('[loadmonth] from: ' + client.name + ' ::: ' + JSON.stringify(data));

		var notification = {};
		if ( isAdmin(client) ) {
			getAdminSettings(client, data.id);
		} else {
			notification = {
				title: "Error!!!",
				message: "You are not authorized to view this info!",
				image: "/images/error.jpg",
				from: "admin"
			};
			client.emit('notification', notification);
			logIt('[emit] to: ' + client.name + ' ::: ' + JSON.stringify(notification));
		}
	});

	client.on('closemonth', function(data){
		logIt('[closemonth] from: ' + client.name + ' ::: ' + JSON.stringify(data));

		var notification = {};
		if ( isAdmin(client) ) {
			var money;
			try {
				money = fs.readFileSync(MONEY_FILE);
				money = JSON.parse(money);
				delete money[data.id];
				fs.writeFile(MONEY_FILE, JSON.stringify(money, null, '\t'), function(err) {
					if(err) {
						return logIt("[ERROR] " + err);
					};
					logIt('MONEY_FILE file was edited!');
					getAdminSettings(client);
				});
			} catch(err){
				logIt(err);
				fs.closeSync(fs.openSync(MONEY_FILE, 'w'));
			}			
		} else {
			notification = {
				title: "Error!!!",
				message: "You are not authorized to close months!",
				image: "/images/error.jpg",
				from: "admin"
			};
			client.emit('notification', notification);
			logIt('[emit] to: ' + client.name + ' ::: ' + JSON.stringify(notification));
		}
	});

	client.on('settings', function(data){
		logIt('[settings] from: ' + client.name + ' ::: ' + JSON.stringify(data));
		if ( isAdmin(client) ) {
			saveSettings(data, client);
		} else {
			notification = {
				title: "Greška!!!",
				message: "You are not authorized to change settings!",
				type: "error",
				from: "admin"
			};
			client.emit('notification', notification);
			logIt('[emit] to: ' + client.name + ' ::: ' + JSON.stringify(notification));
		}
	});

	client.on('refreshMe', function(){
		logIt('[refreshMe] from: ' + client.name);
		if ( isAdmin(client) ) {
			getAdminSettings(client);
			getSettings(client);
			fs.readFile(GROUPS_CONFIG_FILE, function(err, data) {
				if(err) {
					logIt(err);
					client.emit('lunchgroups', {});
					fs.closeSync(fs.openSync(GROUPS_CONFIG_FILE, 'w'));
				} else {
					data = safeJsonParse(GROUPS_CONFIG_FILE, data);
					client.emit('lunchgroups', data);
				}
			});
		} else {
			getSettings(client);
		}
	});

	client.on('getAllClients', function(filter, fn){
		let allClients = [];
		if ( isAdmin(client) ) {
			logIt("Admin requested client list");
			allClients = getConnectedList();
			var clients = {clients: allClients};
			client.emit('messages', clients);
			logIt('[emit] to client:' + client.name + ' ::: ' + JSON.stringify(clients));
		} else {
			logIt("[ERROR] Client list cannot be sent to anyone but admin.");
		}

		// TODO revise if this is necessary
		fn(allClients);
	});

	client.on('newGroup', function(data){
		logIt('[newGroup] from: ' + client.name + ' ::: ' + JSON.stringify(data));
		if ( isAdmin(client) ) {
			try {
				lunchgroups = fs.readFileSync(GROUPS_CONFIG_FILE);
				lunchgroups = JSON.parse(lunchgroups);
				// First add to new group
				if ( !lunchgroups[data.date][data.name] ) {
					lunchgroups[data.date][data.name] = [];
				} 
				// Save when done
				fs.writeFile(GROUPS_CONFIG_FILE, JSON.stringify(lunchgroups, null, '\t'), function(err) {
					if(err) {
						return logIt(err);
					}
					logIt(GROUPS_CONFIG_FILE + " file was saved!");
					notifyAllClients(client);
					getAdminSettings(client);
					getSettings(client);
					client.emit('lunchgroups', getLunchGroups());
				}); 
			} catch (err) {
				logIt(err.toString());
			}
		} else {
			notAdminError(client);
		}
	});

	client.on('deleteGroup', function(data){
		logIt('[deleteGroup] from: ' + client.name + ' ::: ' + JSON.stringify(data));
		if ( isAdmin(client) ) {
			try {
				lunchgroups = fs.readFileSync(GROUPS_CONFIG_FILE);
				lunchgroups = JSON.parse(lunchgroups);
				// First add to new group
				if ( lunchgroups[data.date][data.name] ) {
					delete lunchgroups[data.date][data.name];
				} 
				// Save when done
				fs.writeFile(GROUPS_CONFIG_FILE, JSON.stringify(lunchgroups, null, '\t'), function(err) {
					if(err) {
						return logIt(err);
					}
					logIt(GROUPS_CONFIG_FILE + " file was saved!");
					notifyAllClients(client);
					getAdminSettings(client);
					getSettings(client);
					client.emit('lunchgroups', getLunchGroups());
				}); 
			} catch (err) {
				logIt(err.toString());
			}
		} else {
			notAdminError(client);
		}
	});

	client.on('editGroup', function(data){
		logIt('[editGroup] from: ' + client.name + ' ::: ' + JSON.stringify(data));
		if ( isAdmin(client) ) {
			try {
				lunchgroups = fs.readFileSync(GROUPS_CONFIG_FILE);
				lunchgroups = JSON.parse(lunchgroups);
				// First add to new group
				if ( lunchgroups[data.date][data.to] && lunchgroups[data.date][data.to].length ) {
					lunchgroups[data.date][data.to].splice(data.position, 0, data.name);
				} else {
					if ( !lunchgroups[data.date] ) lunchgroups[data.date] = {};
					lunchgroups[data.date][data.to] = [data.name];
				}
				// Then delete from old group
				var oldIndex = lunchgroups[data.date][data.from].indexOf(data.name);
				if (oldIndex > -1) {
				    lunchgroups[data.date][data.from].splice(oldIndex, 1);
				}
				// Save when done
				fs.writeFile(GROUPS_CONFIG_FILE, JSON.stringify(lunchgroups, null, '\t'), function(err) {
					if(err) {
						return logIt(err);
					}
					logIt(GROUPS_CONFIG_FILE + " file was saved!");
					notifyAllClients(client);
					getAdminSettings(client);
					getSettings(client);
					client.emit('lunchgroups', getLunchGroups());
				}); 
			} catch (err) {
				logIt(err.toString());
			}
		} else {
			notAdminError(client);
		}
	});

	client.on('addMemberToGroup', function(data){
		logIt('[addMemberToGroup] ' + client.name + ' ::: ' + JSON.stringify(data));
		if ( isAdmin(client) ) {
			try {
				lunchgroups = fs.readFileSync(GROUPS_CONFIG_FILE);
				lunchgroups = JSON.parse(lunchgroups);
				lunchgroups[data.date][data.to].push(data.name);
				// Save when done
				fs.writeFile(GROUPS_CONFIG_FILE, JSON.stringify(lunchgroups, null, '\t'), function(err) {
					if(err) {
						return logIt(err);
					}
					logIt(GROUPS_CONFIG_FILE + " file was saved!");
					notifyAllClients(client);
					getAdminSettings(client);
					getSettings(client);
					client.emit('lunchgroups', getLunchGroups());
				}); 
			} catch (err) {
				logIt(err.toString());
			}
		} else {
			notAdminError(client);
		}
	});

	client.on('deleteMemberFromGroup', function(data){
		logIt('[deleteMemberFromGroup] ' + client.name + ' ::: ' + JSON.stringify(data));
		if ( isAdmin(client) ) {
			try {
				lunchgroups = fs.readFileSync(GROUPS_CONFIG_FILE);
				lunchgroups = JSON.parse(lunchgroups);
				var oldIndex = lunchgroups[data.date][data.from].indexOf(data.name);
				if (oldIndex > -1) {
				    lunchgroups[data.date][data.from].splice(oldIndex, 1);
				}
				// Save when done
				fs.writeFile(GROUPS_CONFIG_FILE, JSON.stringify(lunchgroups, null, '\t'), function(err) {
					if(err) {
						return logIt(err);
					}
					logIt(GROUPS_CONFIG_FILE + " file was saved!");
					notifyAllClients(client);
					getAdminSettings(client);
					getSettings(client);
					client.emit('lunchgroups', getLunchGroups());
				}); 
			} catch (err) {
				logIt(err.toString());
			}
		} else {
			notAdminError(client);
		}
	});

	client.on('disconnect', function() {
		logIt('Client: ' + client.name + ' disconnected');
		notifyAdmin();
	});
});

server.listen(PORT, function(){
	logIt('\n');
	logIt('+--------------------------------------------------------+');
	logIt('|       Server started and listening on port ' + PORT + '!       |');
	logIt('+--------------------------------------------------------+');
});

function notAdminError(client) {
	notification = {
		title: "Greška!!!",
		message: "You are not authorized to change settings!",
		type: "error",
		from: "admin"
	};
	client.emit('notification', notification);
	logIt('[emit] to: ' + client.name + ' ::: ' + JSON.stringify(notification));
}

function getConnectedList() {
	let clients = [];
	for ( let client_id of Object.keys(io.sockets.connected) ) {
		var client = {};
		if ( io.sockets.connected[client_id].name ) {
			client = {
				id: client_id,
				name: io.sockets.connected[client_id].name
			};
		} else {
			client = {
				id: client_id,
				name: 'unknown'
			};
		}
		clients.push(client);
	}
	return clients;
}

function addDateToLog() {
	var d = new Date();
	return d.toLocaleString();
}

function logIt(line) {
	fs.appendFile(LOG_FILE, addDateToLog() + " - " + line + '\n', function (err) {
		if (err) throw err;
	});
}

function isAdmin(client) {
	if ( client.name === 'Admin' ) return true;
	else return false;
}

function getLunchGroups() {
	data = "{}";
	try {
		data = fs.readFileSync(GROUPS_CONFIG_FILE, 'utf8');
	} catch (err) {
		logIt(err);
	}
	return safeJsonParse(GROUPS_CONFIG_FILE, data)
}

function notifyAdmin() {
	for ( let client_id of Object.keys(io.sockets.connected) ) {
		if ( io.sockets.connected[client_id].name && io.sockets.connected[client_id].name === 'Admin' ) {
			let allClients = getConnectedList();
			io.sockets.connected[client_id].emit('messages', {clients: allClients});
		}
	}
}

function authenticateAdmin(client) {
	client.emit('login', true);
}

function saveSettings(data, client) {
	if('weekmenu' in data){
		fs.writeFile(WEEK_MENU_FILE, data.weekmenu.content, function(err) {
			if(err) {
				notification = {
					title: "Greška!!!",
					message: "Greška kod spremanja menija! " + err,
					type: "error",
					from: "admin"
				};
				client.emit('notification', notification);
			}
			logIt(WEEK_MENU_FILE + ' file was saved!');
			notification = {
				title: "Menu",
				message: "Izmjene na meniju su spremljene!",
				type: "success",
				from: "admin"
			};
			client.emit('notification', notification);
			notifyAllClients(client);
		}); 
	} else if ('dailynews') {
		fs.writeFile(DAILY_NEWS_FILE, data.dailynews.content, function(err) {
			if(err) {
				notification = {
					title: "Dnevne novosti!!!",
					message: "Greška kod spremanja novosti! " + err,
					type: "error",
					from: "admin"
				};
				client.emit('notification', notification);
			}
			logIt(DAILY_NEWS_FILE + ' file was saved!');
			notification = {
				title: "Dnevne novosti",
				message: "Izmjene na novostima su spremljene!",
				type: "success",
				from: "admin"
			};
			client.emit('notification', notification);
			notifyAllClients(client);
		}); 
	}
}

function notifyAllClients(client) {
	var settings = {};
	try {
		// get week menu
		settings.weekmenu = fs.readFileSync(WEEK_MENU_FILE).toString();
	} catch (err) {
		logIt(err.toString());
		fs.closeSync(fs.openSync(WEEK_MENU_FILE, 'w'));
	}
	try {
		// get daily news
		settings.dailynews = fs.readFileSync(DAILY_NEWS_FILE).toString();
	} catch (err) {
		logIt(err.toString());
		fs.closeSync(fs.openSync(DAILY_NEWS_FILE, 'w'));
	}
	// get lunch groups
	try {
		settings.lunchgroups = fs.readFileSync(GROUPS_CONFIG_FILE).toString();
	} catch (err) {
		logIt("Lunch group FAILED (getSettings)");
		logIt(err.toString());
		fs.closeSync(fs.openSync(GROUPS_CONFIG_FILE, 'w'));
	}
	client.broadcast.emit('messages', {settings: settings});
}

// All clients get this
// Returns all the data required for the plugin to render
// - Daily menu
// - Current group
// - Week menu
function getSettings(client) {
	var settings = {};
	// get week menu
	try {
		settings.weekmenu = fs.readFileSync(WEEK_MENU_FILE).toString();
	} catch (err) {
		logIt(err.toString());
		fs.closeSync(fs.openSync(WEEK_MENU_FILE, 'w'));
	}

	// get daily news
	try {
		settings.dailynews = fs.readFileSync(DAILY_NEWS_FILE).toString();
	} catch (err) {
		logIt(err.toString());
		fs.closeSync(fs.openSync(DAILY_NEWS_FILE, 'w'));
	}
	
	// get lunch groups
	try {
		settings.lunchgroups = fs.readFileSync(GROUPS_CONFIG_FILE).toString();
		logIt("Lunch group OK (getSettings) - " + settings.lunchgroups);
	} catch (err) {
		logIt("Lunch group FAILED (getSettings)");
		logIt(err.toString());
		fs.closeSync(fs.openSync(GROUPS_CONFIG_FILE, 'w'));
	}

	// get active group
	client.emit('activeLunchGroup', activeGroup.name);

	client.emit('messages', {settings: settings});
	
	return settings;
}

// When admin logs in give him all the data for the front end
function getAdminSettings(client, month) {
	var settings = {};
	var data;
	try {
		data = fs.readFileSync(MONEY_FILE);
		data = JSON.parse(data);
		var months = Object.keys(data);

		if ( !month ) {
			month = _.takeRight(months)[0];
			logIt('Mjesec nije definiran. Uzimam zadnji ' + month);
		}

		settings.money = {
			months: months,
			selected: {
				month: month,
				data: data[month]
			}
		};
	} catch (err) {
		logIt(err.toString());
		fs.closeSync(fs.openSync(MONEY_FILE, 'w'));
	}

	client.emit('messages', {settings: settings});
	return settings;
}

function yourMoneyStatus(name) {
	var settings = {};
	var money;
	logIt("NAME CLIENT: "+name);
	var client = getClient(name);
	// get lunch groups
	try {
		money = JSON.parse(fs.readFileSync(MONEY_FILE).toString());
	} catch (err) {
		logIt(err.toString());
		fs.closeSync(fs.openSync(GROUPS_CONFIG_FILE, 'w'));
	}

	settings.owesMoney = false;
	for (var month in money) {
        if(!money.hasOwnProperty(month)) continue;
        (money[month][name] === 0)?settings.owesMoney = true:false;
    }

    if(client) client.emit('messages', {settings: settings});
}

function getClient(name) {
	for ( let client_id of Object.keys(io.sockets.connected) ) {
		logIt(name+"---"+ io.sockets.connected[client_id].name);
		if ( _.isEqual(name, io.sockets.connected[client_id].name ) ) {
			return io.sockets.connected[client_id];
		}
	}
	return undefined;
}

// Let's check if activeGroup is older than CLEAR_GROUP_IN_MINUTES constant because of this scenario
// ---------------------------------------------------------------------
// Let's say time limit to hold last group info is 60min.
// Admin calls group A.
// 45 minutes later Admin calls group B.
// Group A timeout is still on and it will triger in 15 min deleting the group B info which should still be available for the next 45min.
// ---------------------------------------------------------------------
// This check will help clearing group info ONLY for the last notified group.
function clearActiveGroup(admin) {
	if( (activeGroup.time + (CLEAR_GROUP_IN_MINUTES * 60 * 1000)) <= Date.now() ) {
		activeGroup = {name: "", time: undefined};
		admin.broadcast.emit('activeLunchGroup', "");
	}
}

function boolValue(data) {
	return (data == 'true');
}

function safeJsonParse(file, data) {
	try {
		data = JSON.parse(data);
	} catch (error) {
		data = {};
		logIt('Error: Malformed json file - ' + file);
	}
	return data;
}

// TODO interval which will delete old hash files