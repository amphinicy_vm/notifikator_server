const PORT = 3003;

var express = require('express');
var app = express();
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');

var DIR = '/usr/share/Notifikator/gallery/';

if (!fs.existsSync(DIR)){
	fs.mkdirSync(DIR);
}

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "GET, POST, DELETE");
	next();
});

app.use(express.static(path.join(__dirname, 'public')));

app.get('/gallery', function(req, res){
	var files = fs.readdirSync(DIR);
	files.sort();
	files.reverse();
	var count = files.length;
	var offset = (req.query.offset)?req.query.offset:0;
	files = files.slice(offset, Number(offset) + 10);
	var end = ( (Number(offset) + files.length) === count ) ? true : false ;
	res.send({offset: offset, files: files, count: count, end: end});
});

app.delete('/gallery/:filename', function(req, res){
	try {
		fs.unlinkSync(path.join(DIR, req.params.filename));
		var files = fs.readdirSync(DIR);
		var count = files.length;
		res.send({deleted: true, count: count});
	} catch (err) {
		console.log(err);
		res.send({deleted: false});
	}
});

app.post('/upload', function(req, res){
	var form = new formidable.IncomingForm();
	form.multiples = true;
	form.uploadDir = path.join(DIR);

	var files = [];

	form.on('file', function(field, file) {
		var newName = Date.now().toString() + '_' + file.name;
		var newPath = path.join(form.uploadDir, newName);
		fs.rename(file.path, newPath);
		files.push(newName);
	});

	form.on('error', function(err) {
		console.log('error: \n' + err);
	});

	form.on('end', function() {
		res.end(JSON.stringify(files));
	});

	form.parse(req);

});

var server = app.listen(PORT, function(){
	console.log('Gallery available on port ' + PORT + '!');
});
