// This server is just for serving static files.
// In this case it is used only for images in the /usr/share/Notifikator/gallery/ folder
// nginx could be used for this but this way the images can be put on a separate server together with the gallery.js script.
const PORT = 3002;

var path = require('path');
var express = require('express');
var app = express();

var dir = path.join('/usr/share/Notifikator/gallery');

app.use(express.static(dir));

app.listen(PORT, function () {
    console.log('Serving images on port ' + PORT + '!');
});