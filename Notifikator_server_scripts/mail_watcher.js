const MAIL_BACKUP_FOLDER = '/usr/share/Notifikator/emails/';
const LOG_FILE = '/var/log/notifikator_mail.log';
const GROUPS_CONFIG_FILE = "/usr/share/Notifikator/groups.json";
const MONEY_FILE = "/usr/share/Notifikator/money.json";

var MailListener = require("mail-listener2");
var fs = require("fs");
var _ = require('lodash');
var striptags = require('striptags');

var mailListener = new MailListener({
	username: "*******************",
	password: "*******************",
	host: "exchange.amphinicy.com",
	port: 993,
	tls: true,
	connTimeout: 10000,
	authTimeout: 5000,
	debug: logIt,
	tlsOptions: { rejectUnauthorized: false },
	mailbox: "INBOX/Teta Višnja",
	searchFilter: ["UNSEEN"],
	markSeen: true,
	fetchUnreadOnStart: true,
	mailParserOptions: {streamAttachments: true},
	attachments: true,
	attachmentOptions: { directory: "attachments/" }
});

mailListener.start();

mailListener.on("server:connected", function(){
	logIt("Connected to mailbox");
});

mailListener.on("server:disconnected", function(){
	logIt("Disconnected from mailbox");
});

mailListener.on("error", function(err){
	logIt(err);
});

mailListener.on("mail", function(mail, seqno, attributes){
	logIt("email received:");
	logIt("\tSubject - " + mail.subject);
	logIt("\tFrom - " + mail.from[0].name);
	logIt("\temail - " + mail.from[0].address);

	if (!fs.existsSync(MAIL_BACKUP_FOLDER)){
		fs.mkdirSync(MAIL_BACKUP_FOLDER);
	}
	// if ( mail.subject === "Raspored za ručak") {
	if ( mail.from[0].name === "Teta Visnja") {
		var filename = saveName();
		fs.writeFile(MAIL_BACKUP_FOLDER + "/" + filename, mail.html, function(err) {
			if(err) {
					return logIt(err);
			}
			logIt("email is saved to - " + MAIL_BACKUP_FOLDER + "/" + filename);
		});

		parseEmail(mail);
	} 
});

// Not doing anything with the attachment at the moment
mailListener.on("attachment", function(attachment){
	logIt(attachment.path);
});

// filename is in format YYYY_MM_DD-hhmmss-randomString
function saveName () {
	var d = new Date();
	var filename = d.getFullYear() + "_" + doubleDigit( d.getMonth() + 1 ) + "_" + doubleDigit( d.getDate() ) + "-" + doubleDigit(d.getHours()) + "" + doubleDigit(d.getMinutes()) + "" + doubleDigit(d.getSeconds()) + "-" + randomString();
	return filename;
}

function randomString() {
	return Math.random().toString(36).substr(2);
}

function checkEmpty(value) {
	return value != '';
}

function doubleDigit (number) {
	if (number < 10) {
		return ("0" + number);
	}
	return number;
}

function addDateToLog() {
	var d = new Date();
	return d.toLocaleString();
}

function logIt(line) {
	fs.appendFile(LOG_FILE, addDateToLog() + " - " + line + '\n', function (err) {
		if (err) throw err;
	});
}

// This method is specific to Amphinicy "Raspored za ručak" email.
// You should rewrite this method to your specific needs.
// Initial plan was to use html parser modules but due to some small tag issues that didn't work so step by step aproach was implemented.
function parseEmail ( mail ) {
	logIt("========== PARSING EMAIL START ==========");
	logIt("\tSubject - " + mail.subject);
	logIt("\tFrom - " + mail.from[0].name);
	logIt("\temail - " + mail.from[0].address);
	logIt("\HTML - " + mail.html);

	var schedule = {};
	var days = [];

	// skipping css i.e. first 1000 characters
	var startIndex = mail.html.indexOf('table_container', 1000);
	startIndex = mail.html.indexOf('<th', startIndex);
	var endIndex = mail.html.indexOf('<tr', startIndex);
	var daysHeader = mail.html.substring(startIndex, endIndex);

	var regX = /(\d+-\d+-\d+,\w+)/g;
	days = daysHeader.match(regX);

	if ( !days ) days = [];

	logIt("======== DAYS ========");
	logIt(" # of days: " + days.length);
	logIt("----------------------");
	for (var i = 0; i < days.length; i++) {
		logIt(" " + days[i]);
		schedule[days[i]] = {};
	}
	logIt("----------------------");

	// We need this for the money.json config
	var namesInMail = {};

	for (day in days) {
		if (days.hasOwnProperty(day)){
			startIndex = mail.html.indexOf('gridtable', endIndex);
			startIndex = mail.html.indexOf('<th', startIndex);
			endIndex = mail.html.indexOf('<tr', startIndex);
			var dayHeader = mail.html.substring(startIndex, endIndex);
			var groupsInDay = striptags(dayHeader, [], ';').split(";");
			groupsInDay = groupsInDay.filter(checkEmpty);
			logIt('# of groups in a day: ' + groupsInDay.length);

			// add groups to schedule
			schedule[days[day]] = _.keyBy(groupsInDay, function(value) {
				return value;
			});

			// get all names and put it in array. Then filter it out and assign to groups.
			startIndex = mail.html.indexOf('<tr', endIndex);
			endIndex = mail.html.indexOf('</table', startIndex);
			var dayBody = mail.html.substring(startIndex, endIndex);
			var namesInDay = striptags(dayBody, [], ';').split(";");
			namesInDay = namesInDay.filter(checkEmpty);

			var dayChunks = _.chunk(namesInDay, groupsInDay.length);

			for (var i = 0; i < dayChunks.length; i++) {
				for (var j = 0; j < dayChunks[i].length; j++) {
					if( ! Array.isArray(schedule[days[day]][groupsInDay[j]]) ) {
						schedule[days[day]][groupsInDay[j]] = [];
					}
					schedule[days[day]][groupsInDay[j]].push(dayChunks[i][j]);

					// I need this later to add it to the right month in money.json
					if( !(days[day] in namesInMail) ) {
						namesInMail[days[day]] = [];
					}
					namesInMail[days[day]].push(dayChunks[i][j]);
				}
			}
		}
	}

	logIt("-------- schedule ---------");
	logIt(schedule);

	fs.writeFile(GROUPS_CONFIG_FILE, JSON.stringify(schedule, null, '\t'), function(err) {
		if(err) {
			return logIt(err);
		}
		logIt(GROUPS_CONFIG_FILE + " file was saved!");
	}); 

	// POPULATE money.json
	// Add people to money.json if they are not already there
	var money = {};
	if (fs.existsSync(MONEY_FILE)) {
		money = JSON.parse(fs.readFileSync(MONEY_FILE, 'utf8'));
	}

	logIt('Names in mail');
	logIt(namesInMail);

	_.each(namesInMail, function(people, date) {
		// take the date from key 
		var dateArray = date.match(/(\d+)-(\d+)/);
		var monthKey = doubleDigit(parseInt(dateArray[2])) + '/' + dateArray[1];

		var currentMonthPeople = {};
		if ( money[monthKey] ) {
			currentMonthPeople = money[monthKey];
		}

		_.each(namesInMail[date], function(person) {
			if ( !(person in currentMonthPeople) ) {
				logIt('ADD: ' + person);
				currentMonthPeople[person] = 0;
			}
		});

		money[monthKey] = currentMonthPeople;
	});

	logIt(money);

	fs.writeFile(MONEY_FILE, JSON.stringify(money, null, '\t'), function(err) {
		if(err) {
			return logIt(err);
		}
		logIt(MONEY_FILE + " file was saved!");
	}); 
	// POPULATE money.json END
	logIt("-------- PARSING EMAIL DONE ---------");
}